import React from "react";
import { StyledRow } from "./style";

type RowProps = {
    children: JSX.Element | JSX.Element[];
};

function Row({ children }: RowProps) {
    return <StyledRow>{children}</StyledRow>;
}

export default Row;
