import React from "react";
import { H1 } from "./style";

type TitleProps = {
    children: string | string[];
};

function Title({ children }: TitleProps) {
    return <H1>{children}</H1>;
}

export default Title;
