import styled from "styled-components";

export const H1 = styled.h1`
    font-size: 48px;
    letter-spacing: -4px;
    width: 500px;
    color: black;
    font-weight: bold;
    text-align: center;
    margin-bottom: 2rem;
`