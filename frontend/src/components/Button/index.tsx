import React from "react";
import { StyledButton } from "./style";

type ButtonProps = {
    onClickFn: () => void;
    children: string;
};

function Button({ children, onClickFn }: ButtonProps) {
    return <StyledButton onClick={onClickFn}>{children}</StyledButton>;
}

export default Button;
