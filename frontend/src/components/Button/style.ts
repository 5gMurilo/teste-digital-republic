import styled from "styled-components";

export const StyledButton = styled.button`
    padding: 8px 24px;
    max-height: 100px;
    border: 3px dotted black;
    text-transform: capitalize;
    font-size: 48px;
    border-radius: 12px;
    margin-block: 40px;
    background-color: transparent;
    cursor: pointer;
    font-weight: 500;
`