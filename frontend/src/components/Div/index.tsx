import React from 'react'
import { StyledDiv } from './style'

type DivProps = {
    children: JSX.Element | JSX.Element[]
    direction: string
}

function Div({children, direction}:DivProps) {
  return (
    <StyledDiv direction={direction}>
      {children}
    </StyledDiv>
  )
}

export default Div
