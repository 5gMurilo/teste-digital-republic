import styled from "styled-components";

export const StyledDiv = styled.div<{direction: string}>`
    display: flex;
    flex-direction: ${props => props.direction || 'row'};
    align-items: center;
    justify-content: center;
    height: max-content;
`