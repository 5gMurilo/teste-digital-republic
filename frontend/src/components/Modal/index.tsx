import React, { useState } from "react";
import { ModalContainer, ModalHeader, StyledModal } from "./style";
import Title from "../Title";
import Field from "../Field";
import Button from "../Button";
import Row from "../Row";
import { IoIosClose } from "react-icons/io";
import { FieldSize } from "../Field/enum";

import wallValidation from "../../util/validateWall";
import { WallType } from "../../types/wall";

type ModalProps = {
    visible: boolean;
    closeModal: () => void;
    wall: WallType;
    saveWall: (newWall: WallType) => void;
    walls: WallType[];
};

const Modal: React.FC<ModalProps> = ({
    visible,
    closeModal,
    wall,
    walls,
    saveWall,
}) => {
    if (walls.length > 0) {
        const isWallAlreadyPresent = walls.filter(
            (existingWalls) => existingWalls.wallId === wall.wallId
        );
        if (isWallAlreadyPresent.length > 0) {
            wall = isWallAlreadyPresent[0];
        }
    }

    const [currentWall, setCurrentWall] = useState<WallType>(wall);

    return (
        <ModalContainer $visible={visible}>
            <StyledModal>
                <ModalHeader>
                    <IoIosClose
                        size={50}
                        onClick={() => {
                            setCurrentWall(wall);
                            closeModal();
                        }}
                        cursor={"pointer"}
                    />
                    <Title>
                        Propriedades da parede{" "}
                        {currentWall.wallId.toString() ?? ""}:{" "}
                    </Title>
                </ModalHeader>

                <Field
                    step="0.1"
                    fieldtext="Largura da parede:"
                    value={currentWall.width}
                    updateValue={(width: number) =>
                        setCurrentWall({ ...currentWall, width })
                    }
                />
                <Field
                    step="0.1"
                    fieldtext="Altura da parede:"
                    value={currentWall.height}
                    updateValue={(height: number) =>
                        setCurrentWall({ ...currentWall, height })
                    }
                />

                <Row>
                    <Field
                        step="1"
                        fieldtext="Qtd. de portas:"
                        fieldsize={FieldSize.Small}
                        value={currentWall.door?.quantity ?? 0}
                        updateValue={(n) => {
                            if (currentWall.door === undefined) {
                                setCurrentWall({
                                    ...currentWall,
                                    door: {
                                        height: 1.9,
                                        width: 0.8,
                                        quantity: 1
                                    },
                                });
                            } else {
                                setCurrentWall({
                                    ...currentWall,
                                    door: {
                                        ...currentWall.door,
                                        quantity: n
                                    },
                                });
                            }
                        }}
                    />
                    <Field
                        step="1"
                        fieldtext="Qtd. de janelas:"
                        fieldsize={FieldSize.Small}
                        value={currentWall.window?.quantity ?? 0}
                        updateValue={(n) => { 
                            if (currentWall.window === undefined) {
                                setCurrentWall({
                                    ...currentWall,
                                    window: {
                                        height: 1.9,
                                        width: 0.8,
                                        quantity: 1
                                    },
                                });
                            } else {
                                setCurrentWall({
                                    ...currentWall,
                                    window: {
                                        ...currentWall.window,
                                        quantity: n
                                    },
                                });
                            }
                        }}
                    />
                </Row>

                <Button
                    onClickFn={() => {
                        const isValid = wallValidation(currentWall);
                        isValid
                            ? saveWall(currentWall)
                            : window.alert("A parede é inválida");

                        closeModal();
                    }}
                >
                    Salvar
                </Button>
            </StyledModal>
        </ModalContainer>
    );
};

export default Modal;
