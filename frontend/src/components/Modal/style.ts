import styled from "styled-components";

export const StyledModal = styled.div`
    min-width: 600px;
    margin: auto;
    display: flex;
    flex-direction: column;
    row-gap: 36px;
    height: max-content;
    padding: 40px 16px;
    border-radius: 32px;
    background-color: #fffbf8;
    z-index: 20;
    display: flex;
    align-items: center;
    text-align: center;
`;

export const ModalContainer = styled.data<{ $visible: boolean }>`
    width: 100vw;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.7);
    z-index: 10;
    position: absolute;
    display: ${(props) => (props.$visible === false ? "none" : "flex")};
`;

export const ModalHeader = styled.div`
    width: 100%;
    display: flex;
    flex-direction: row-reverse;
`;
