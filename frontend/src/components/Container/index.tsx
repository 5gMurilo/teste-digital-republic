import React from 'react'
import { MyContainer } from './style'

type ContainerProps = {
    children: JSX.Element | JSX.Element[]
}

function Container({ children }:ContainerProps) {
  return (
    <MyContainer>
      {children}
    </MyContainer>
  )
}

export default Container
