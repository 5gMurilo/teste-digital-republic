import styled from "styled-components";

export const MyContainer = styled.div`
    margin: auto;
    width: 100vw;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    row-gap: 32px;
`;