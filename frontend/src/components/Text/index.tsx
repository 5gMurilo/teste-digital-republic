import React from 'react'
import { P } from './style'

type TextProps = {
    children: string;
    width?:string
};

function Text({children, width}:TextProps) {
  return (
    <P width={width}>
      {children}
    </P>
  )
}

export default Text
