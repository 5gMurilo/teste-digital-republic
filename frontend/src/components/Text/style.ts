import styled from "styled-components";

export const P = styled.p<{width?: string}>`
    font-size: 24px;
    font-weight: 500;
    width: ${props => props.width || 'auto'};
    height: max-content;
    text-align: center;
    margin: auto 1rem;
`;