import React from "react";
import { FieldContainer, FieldInput, FieldText } from "./style";
import { FieldSize } from "./enum";

type FieldProps = {
    fieldtext: string;
    fieldsize?: FieldSize;
    value: number;
    updateValue: (n: number) => void;
    step: string;
};

const Field: React.FC<FieldProps> = ({
    fieldtext,
    fieldsize,
    value,
    updateValue,
    step,
}) => {
    return (
        <FieldContainer $fieldsize={fieldsize}>
            <FieldText $fieldsize={fieldsize}>{fieldtext}</FieldText>
            <FieldInput
                type="number"
                $fieldsize={fieldsize}
                value={value}
                step={step}
                min={0}
                onChange={(n) => {
                    updateValue(parseFloat(n.target.value));
                }}
            />
        </FieldContainer>
    );
};

export default Field;
