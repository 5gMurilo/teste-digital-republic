import styled from "styled-components";
import { FieldSize } from "./enum";

export const FieldContainer = styled.div<{ $fieldsize?: FieldSize }>`
    display: flex;
    flex-direction: row;
    column-gap: ${(props) =>
        props.$fieldsize === FieldSize.Small ? "12px" : "24px"};
    width: 100%;
    align-items: center;
    justify-content: center;
    text-align: justify;
`;

export const FieldText = styled.p<{ $fieldsize?: FieldSize }>`
    width: ${(props) =>
        props.$fieldsize === FieldSize.Small ? "max-content" : "290px"};
    font-size: 28px;
    color: #0f0f0f;
    font-weight: bold;
    text-transform: capitalize;
`;

export const FieldInput = styled.input<{ $fieldsize?: FieldSize }>`
    width: ${(props) =>
        props.$fieldsize === FieldSize.Small ? "50px" : "300px"};
    border: 2px dotted black;
    background-color: transparent;
    transition: 0.2s ease-out;
    border-radius: 8px;
    font-size: 32px;
    padding: 4px 8px;
    text-align: ${(props) =>
        props.$fieldsize === FieldSize.Small ? "center" : "justify"};

    &:focus {
        transition: 0.2s ease-in;
        background-color: rgba(255, 255, 255, 0.7);
        border-style: solid;
        border-radius: 12px;
        outline: none;
    }
`;
