import React from "react";
import { HorizontalWall, VerticalWall } from "./style";
import { Orientation } from "./enum";

type WallProps = {
    id: number;
    orientation: Orientation;
    hasValue: boolean;
    onClickFn: () => void;
};

function Wall({ id, orientation, onClickFn }: WallProps) {
    return orientation === Orientation.Vertical ? (
        <VerticalWall onClick={onClickFn}>{id}</VerticalWall>
    ) : (
        <HorizontalWall onClick={onClickFn}>{id}</HorizontalWall>
    );
}

export default Wall;
