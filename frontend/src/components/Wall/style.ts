import styled from "styled-components";

export const VerticalWall = styled.div`
    width: 20px;
    height: 240px;
    border: 3px dotted black;
    border-radius: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: 0.2s ease-out;
    cursor: pointer;

    &:hover {
        border-style: solid;
        border-color: white;
        background-color: rgba(102, 191, 255, 0.5);
        box-shadow: 0 4px 5px rgba(0, 0, 0, 0.2);
        transition: 0.2s ease-out;
        color: white;
    }
`;

export const HorizontalWall = styled.div`
    width: 240px;
    height: 20px;
    border: 3px dotted black;
    border-radius: 4px;
    display: flex;
    align-items: center;
    justify-content: center;
    transition: 0.5s ease-out;
    cursor: pointer;

    &:hover {
        border-style: solid;
        border-color: white;
        background-color: rgba(255, 125, 102, 0.5);
        box-shadow: 0 4px 5px rgba(0, 0, 0, 0.2);
        transition: 0.2s ease-in-out;
        color: white;
    }
`;
