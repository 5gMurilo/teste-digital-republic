export interface WallType {
    wallId: number;
    width: number;
    height: number;
    door?: Door;
    window?: Window;
}

export interface Door {
    quantity: number;
    width: number;
    height: number;
}
export interface Window {
    quantity: number;
    width: number;
    height: number;
}
