import { WallType } from "../types/wall";

export default function wallValidation(wall: WallType): boolean {
    const squareMeters = wall.width * wall.height;

    if (squareMeters < 1 || squareMeters > 50) {
        return false;
    }

    if (wall.door !== undefined || wall.window !== undefined) {
        const doorSquareMeters =
            wall.door !== undefined ? wall.door.height * wall.door.width : 0;
        const windowSquareMeters =
            wall.window !== undefined
                ? wall.window.height * wall.window.width
                : 0;

        if (wall.door !== undefined && wall.window !== undefined) {
            if (wall.height - wall.door.height >= 0.3) {
                return (
                    doorSquareMeters + windowSquareMeters <= squareMeters / 2
                );
            }
            return false;
        } else if (wall.door !== undefined) {
            if (wall.height - wall.door.height >= 0.3) {
                return doorSquareMeters <= squareMeters / 2;
            }
            return false;
        }

        return windowSquareMeters <= squareMeters / 2;
    }
    return squareMeters >= 1 && squareMeters <= 50;
}
