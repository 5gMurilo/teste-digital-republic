import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    *{
        height: 100%;
        margin: 0;
        padding: 0;
    }
    
    body {
        font-family: "Montserrat", sans-serif;
        font-optical-sizing: auto;
    }
`;
