import React, { useState } from "react";
import Container from "./components/Container";
import Title from "./components/Title";
import Wall from "./components/Wall";
import { Orientation } from "./components/Wall/enum";
import Div from "./components/Div";
import Text from "./components/Text";
import Button from "./components/Button";
import Modal from "./components/Modal";
import { WallType } from "./types/wall";
import api from "./api/axios";
import axios from "axios";

const initialState: WallType[] = [
    {
        wallId: 1,
        height: 0,
        width: 0,
        door: undefined,
        window: undefined,
    },
    {
        wallId: 2,
        height: 0,
        width: 0,
        door: undefined,
        window: undefined,
    },
    {
        wallId: 3,
        height: 0,
        width: 0,
        door: undefined,
        window: undefined,
    },
    {
        wallId: 4,
        height: 0,
        width: 0,
        door: undefined,
        window: undefined,
    },
];

function App() {
    const [walls, setWalls] = useState<WallType[]>([]);
    const [modalState, setModalState] = useState<{
        state: boolean;
        wall: WallType;
    }>({ state: false, wall: initialState[0] });

    function saveWall(newWall: WallType) {
        const isWallExistent = walls.find(
            (wall) => wall.wallId === newWall.wallId
        );

        if (isWallExistent !== undefined) {
            setWalls((prevWalls) =>
                prevWalls
                    .filter((wall) => wall.wallId !== newWall.wallId)
                    .concat([newWall])
            );
        } else {
            setWalls((prevState) => [...prevState, newWall]);
        }
    }

    async function submitWalls() {
        if (walls.length < 4) {
            window.alert(
                `resta(m) ${
                    4 - walls.length
                } parede(s). Preencha a(s) parede(s) restante(s)`
            );
        }

        await api
            .post("http://localhost:4000/calculate", walls)
            .then((r) => window.alert(r.data.result))
            .catch((e) => window.Error(e));
    }

    return (
        <Container>
            <Div direction="Column">
                <Title>Calculadora de latas de tinta</Title>
                <Div direction="Column">
                    <Wall
                        hasValue={false}
                        orientation={Orientation.Horizontal}
                        id={initialState[1].wallId}
                        onClickFn={() =>
                            setModalState({
                                state: true,
                                wall: initialState[1],
                            })
                        }
                    />
                </Div>
                <Div direction="row">
                    <Wall
                        hasValue={false}
                        orientation={Orientation.Vertical}
                        id={initialState[0].wallId}
                        onClickFn={() => {
                            setModalState({
                                state: true,
                                wall: initialState[0],
                            });
                        }}
                    />
                    <Text width={"360px"}>
                        Selecione uma das paredes e insira as informações
                        necessárias para o cálculo
                    </Text>
                    <Wall
                        hasValue={false}
                        orientation={Orientation.Vertical}
                        id={initialState[2].wallId}
                        onClickFn={() => {
                            setModalState({
                                state: true,
                                wall: initialState[2],
                            });
                        }}
                    />
                </Div>
                <Div direction="Column">
                    <Wall
                        hasValue={false}
                        orientation={Orientation.Horizontal}
                        id={initialState[3].wallId}
                        onClickFn={() => {
                            setModalState({
                                state: true,
                                wall: initialState[3],
                            });
                        }}
                    />
                </Div>
                <Button onClickFn={submitWalls}>calcular</Button>
            </Div>

            {modalState.state ? (
                <Modal
                    saveWall={saveWall}
                    visible={modalState.state}
                    closeModal={() =>
                        setModalState({ state: false, wall: initialState[0] })
                    }
                    wall={modalState.wall}
                    walls={walls}
                />
            ) : (
                <></>
            )}
        </Container>
    );
}

export default App;
