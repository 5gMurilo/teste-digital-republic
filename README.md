# Teste - Digital Republic

Olá! seja muito bem-vindo(a), muito prazer eu sou o Murilo e esse é meu teste para a empresa Digital Republic!

Esse projeto tem como objetivo demonstrar minhas habilidades nas tecnologias condizentes com a vaga  - Pessoa Desenvolvedora Fullstack Júnior - React/NodeJS

O projeto em questão é uma calculadora que realiza o calculo da quantidade de latas de tinta necessárias para a pintura de uma sala baseado nas dimensões a serem informadas pelo usuário.

Assim como solicitado no descritivo do teste, não é levado em consideração teto e/ou piso da sala, apenas os metros quadrados das paredes.

## Tecnologias 
As tecnologias utilizadas no projeto foram:
- React
- Express
- Jest
- Typescript
- Docker

## Execução
Para executar o projeto, é necessário que você tenha instalado o [Docker](https://www.docker.com/) e o [Git](https://git-scm.com/) previamente.

É possível executar o projeto sem o Docker, desde que você tenha o [Node.JS](https://nodejs.org/en) instalado na sua máquina.

### Como rodar o projeto com o docker?
Clone o repositório utilizando o comando ``` git clone https://gitlab.com/5gMurilo/teste-digital-republic```.

Com o processo de clonagem do repositório finalizado, abra a pasta do projeto no seu terminal e digite o comando ```docker compose up -d```.

Logo após a construção dos containers necessários, no seu navegador acesse [http://localhost:3000/](http://localhost:3000/) e siga as instruções.

### Como rodar o projeto sem o docker?
Clone o repositório utilizando o comando ``` git clone https://gitlab.com/5gMurilo/teste-digital-republic```.

Com o processo de clonagem do repositório finalizado, abra a pasta do projeto no seu terminal, acesse a pasta **frontend** através do comando ```cd frontend``` e digite o comando ``` npm start ```. Lembre-se que rodando com o npm start, o projeto não estará otimizado para produção!

Após isso, abra outro terminal, acesse a pasta do projeto, mas dessa vez, acesse a pasta **backend** através do comando ```cd backend``` e digite o comando ```npm run dev```. Assim como no front-end, rodando com esse comando o projeto não estará otimizado para produção.

# Fim!
Gostaria de agradecer a disponibilidade e a oportunidade que foi me dada pelo time da Digital Republic, caso tenham algum feedback de melhoria, sinta-se a vontade para criar um tópico nesse readme!