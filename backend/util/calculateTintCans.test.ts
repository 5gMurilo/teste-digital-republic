import { test, expect } from "@jest/globals";
import { Wall } from "../entity/wall";
import CalculateTintCans from "./calculateTintCans";

test("Return necessary tint cans", () => {
    const wallWithWindow = new Wall({
        height: 2.2,
        width: 6,
        wallId: 1,
        window: {
            height: 1.2,
            width: 2,
            quantity: 1,
        },
    });

    const wallWithDoorAndWindow = new Wall({
        height: 2.2,
        width: 6,
        wallId: 2,
        window: {
            height: 1.2,
            width: 2,
            quantity: 1,
        },
        door: {
            height: 1.9,
            width: 0.8,
            quantity: 1,
        },
    });

    const wall = new Wall({
        height: 2.2,
        width: 6,
        wallId: 2,
    })

    const walls: Wall[] = [wall, wall, wallWithWindow, wallWithDoorAndWindow]

    const result = CalculateTintCans(walls)

    expect(result.length).toBeGreaterThan(16);
});
