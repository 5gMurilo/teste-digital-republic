import { Wall } from "../entity/wall";

export default function wallValidation(wall: Wall): boolean {
  const squareMeters = wall.getWidth * wall.getHeight;
  
  if (squareMeters < 1 || squareMeters > 50) {
    return false;
  }

  if (wall.getDoor != undefined || wall.getWindow != undefined) {
    const doorSquareMeters =
      wall.getDoor != undefined ? wall.getDoor.height * wall.getDoor.width : 0;
    const windowSquareMeters =
      wall.getWindow != undefined
        ? wall.getWindow.height * wall.getWindow.width
        : 0;

    if (wall.getDoor != undefined && wall.getWindow != undefined) {
      if (wall.getHeight - wall.getDoor!.height >= 0.3) {
        return doorSquareMeters + windowSquareMeters <= squareMeters / 2;
      }
      return false;
    } else if (wall.getDoor != undefined) {
      if (wall.getHeight - wall.getDoor.height >= 0.3) {
        return doorSquareMeters <= squareMeters / 2;
      }
      return false;
    }

    return windowSquareMeters <= squareMeters / 2;
  }
  return squareMeters >= 1 && squareMeters <= 50;
}
