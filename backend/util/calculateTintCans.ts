import { Wall } from "../entity/wall";

export default function CalculateTintCans(walls: Wall[]): string {
    let necessaryLiters: number = 0;
    let necessaryCans: { [key: string]: number } = {
        "18": 0,
        "3.6": 0,
        "2.5": 0,
        "0.5": 0,
    };

    walls.map(
        (wall) => necessaryLiters += parseFloat(((wall.getWallSquareMeters - (wall.getDoorSquareMeters + wall.getWindowSquareMeters)) / 5).toFixed(2))
    );

    console.log(necessaryLiters);

    Object.keys(necessaryCans).forEach((can, i) => {
        const canLiters: number = parseFloat(can);
        
        while (necessaryLiters >= canLiters) {
            necessaryCans[can]++;
            necessaryLiters -= canLiters;
        }

        while(Object.keys(necessaryCans).length - 1 == i && necessaryLiters >= 0.1){
            necessaryCans[can]++;
            necessaryLiters -= canLiters;
        }
    });

    console.log(necessaryCans);

    let result = "Você precisa de ";
    Object.keys(necessaryCans).forEach((canSize, i) => {
        if (necessaryCans[canSize] > 0) {
            result += `${necessaryCans[canSize]} lata(s) de ${canSize}`;
            if (i < Object.keys(necessaryCans).length - 1) {
                result += " + ";
            }
        }
    });

    if (necessaryLiters < 0) { 
        result += `. Haverá uma sobra de ${parseFloat(necessaryLiters.toFixed(2)) * -1} ml de tinta.`
    }

    return result;
}
