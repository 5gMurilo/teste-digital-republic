import express from "express";
import cors, { CorsOptions } from "cors";
import * as dotenv from "dotenv";
import router from "./routes";
import morgan from "morgan";

dotenv.config();

const app = express();

const port = process.env.PORT || 3000;
const corsOptions: CorsOptions = {
  origin: "*",
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(morgan("tiny"));

app.listen(port, () => console.log(`http://localhost:${port}/`));

app.use(router);
