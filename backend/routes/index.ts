import { Router, Request, Response } from "express";
import { Wall, WallProps } from "../entity/wall";
import wallValidation from "../util/wallValidation";
import CalculateTintCans from "../util/calculateTintCans";

const router = Router();

router.get("/", (_, res: Response) => {
  res.send("Olá mundo!");
});

router.post("/calculate", async (req: Request, res: Response) => {
  const walls: Wall[] = req.body.map((props: WallProps) => new Wall(props));
  const validationErrors: string[] = []

  walls.map((v,i) => {
    if (!wallValidation(v)) { 
      validationErrors.push(`A parede ${i + 1} é inválida`);
    }
  });

  if (validationErrors.length > 0) { 
    return res.status(500).json({ "errors": validationErrors });
  }

  const result = CalculateTintCans(walls)

  return res.json({'result': result});
});

export default router;
