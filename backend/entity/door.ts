export interface Door {
  quantity: number;
  width: number;
  height: number;
}
