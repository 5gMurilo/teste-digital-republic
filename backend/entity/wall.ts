import { Door } from "./door";
import { Window } from "./window";

export interface WallProps {
  wallId: number;
  width: number;
  height: number;
  door?: Door;
  window?: Window;
}

export class Wall {
  private props: WallProps;

  constructor(props: WallProps) {
    this.props = props;
  }

  get getWidth(): number {
    return this.props.width;
  }

  get getHeight(): number {
    return this.props.height;
  }

  get getHasDoor(): boolean {
    return this.props.door != undefined;
  }

  get getHasWindow(): boolean {
    return this.props.window != undefined;
  }

  get getDoor(): Door | undefined {
    return this.props.door;
  }

  get getWindow(): Window | undefined {
    return this.props.window;
  }

  get getWallSquareMeters(): number {
    return this.props.width * this.props.height;
  }

  get getDoorSquareMeters(): number {
    if (this.props.door == undefined){
      return 0;
    }
    return this.props.door.height * this.props.door.width ;
  }
  get getWindowSquareMeters(): number {
    if (this.props.window == undefined){
      return 0;
    }
    return this.props.window.height * this.props.window.width ;
  }
}
