import { test, expect } from "@jest/globals";
import { Wall, WallProps } from "./wall";
import wallValidation from "../util/wallValidation";

test("Return valid wall without door and window", () => {
  const wall = new Wall({
    height: 1.0,
    width: 1.0,
    wallId: 1,
  });

  const wall2 = new Wall({
    height: 5.0,
    width: 10.0,
    wallId: 2,
  });

  expect(wallValidation(wall)).toEqual(true);
  expect(wallValidation(wall2)).toEqual(true);
});

test("Return valid wall with door only", () => {
  const validWall = new Wall({
    height: 2.3,
    width: 5.5,
    wallId: 2,
    door: {
      height: 1.9,
      width: 0.8,
      quantity: 1
    },
  });

  expect(wallValidation(validWall)).toEqual(true);
});

test("Return valid wall with window only", () => {
  const wall = new Wall({
    height: 1.0,
    width: 1.0,
    wallId: 1,
    window: {
      height: 1.2,
      width: 2,
      quantity: 1
    },
  });

  const validWall = new Wall({
    height: 2.2,
    width: 5.5,
    wallId: 2,
    window: {
      height: 1.2,
      width: 2,
      quantity: 1
    },
  });

  expect(wallValidation(wall)).toEqual(false);
  expect(wallValidation(validWall)).toEqual(true);
});

test("Return valid wall with door and window", () => {
  const wall = new Wall({
    height: 1.0,
    width: 1.0,
    wallId: 1,
    window: {
      height: 1.2,
      width: 2,
      quantity: 1
    },
    door: {
      height: 1.9,
      width: 0.8,
      quantity: 1
    },
  });

  const validWall = new Wall({
    height: 2.2,
    width: 5.5,
    wallId: 2,
    window: {
      height: 1.2,
      width: 2,
      quantity: 1
    },
    door: {
      height: 1.9,
      width: 0.8,
      quantity: 1
    },
  });

  expect(wallValidation(wall)).toEqual(false);
  expect(wallValidation(validWall)).toEqual(true);
});

test("Return valid wall interface with door and window", () => {

  const wallprops: WallProps = {
    height: 1.0,
    width: 5.5,
    wallId: 3,
    window: {
      quantity: 1,
      width: 2,
      height: 1.2,
    },
    door: {
      quantity: 1,
      width: 0.8,
      height: 1.9,
    },
  };

  const wall: Wall = new Wall(wallprops)

  expect(wallValidation(wall)).toEqual(false);
});
