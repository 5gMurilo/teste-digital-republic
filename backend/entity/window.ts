export interface Window {
  quantity: number;
  width: number;
  height: number;
}
